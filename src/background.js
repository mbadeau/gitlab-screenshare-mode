/**
 * ensure that styles are injected _before_ DOM Content is loaded in order
 * to prevent flickering of content
 */
chrome.webNavigation.onCommitted.addListener(
  function (data) {
    chrome.tabs.sendMessage(data.tabId, { action: "injectStyle" });
  },
  { url: [{ hostPrefix: "gitlab.com" }] }
);
