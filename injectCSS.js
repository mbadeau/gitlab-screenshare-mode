/**
 * Mock some functions
 */
const noop = () => ({});
global.window = {
  addEventListener: noop,
  module: module,
};
global.document = {
  createElement: noop,
  body: { appendChild: noop },
};
global.chrome = {
  runtime: {
    onMessage: {
      addListener: noop,
    },
  },
  storage: {
    local: {
      get: noop,
    },
  },
};

const fs = require("fs");
const path = require("path");

const JSFile = path.join(__dirname, "./src/inject.js");

const cleaners = require(JSFile);

const CSSFile = path.join(__dirname, "./inject.css");

const CSSContent = fs.readFileSync(CSSFile, "utf-8");
const JSContent = fs.readFileSync(JSFile, "utf-8");

const selectors = cleaners
  .flatMap((cleaner) => {
    if (!cleaner.selector) {
      return [];
    }

    return [
      `${cleaner.selector}:not(.screenshare-mode-checked)`,
      cleaner.childSelector,
    ]
      .filter(Boolean)
      .join(" ");
  })
  .sort()
  .join(",\n");

const css = CSSContent.replace(
  "/* selectors */",
  selectors.length > 0 ? ",\n" + selectors : ""
);

const js = JSContent.replace(
  /\/\*.+?inject:start[\s\S]+?inject:end.+?\*\//gm,
  "/* inject:start */\n" + css + "\n/* inject:end */"
);

fs.writeFileSync(JSFile, js);
